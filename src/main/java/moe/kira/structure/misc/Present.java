package moe.kira.structure.misc;

public interface Present {
    public default boolean has() {
        return isPresent();
    }
    
    public boolean isPresent();
    
    public boolean isEmpty();
}
